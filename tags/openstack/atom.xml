<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Sandro Mathys | Blog</title>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/mathys-io.css" rel="stylesheet" />
    <link href="/css/navbar.css" rel="stylesheet" />
    <link href="/css/blog.css" rel="stylesheet" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#ffffff">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="alternate" type="application/atom+xml" title="Atom Feed" href="/atom.xml" />
  </head>
  <body>

<nav class="navbar navbar-full navbar-light hidden-print">
  <div class="nav nav-tabs">

      <a href="https://photography.mathys.io/" class="nav-item nav-link disabled" title="Coming soon!">Photography</a>

      <h1><a href="https://blog.mathys.io/" class="nav-item nav-link active">Blog<span class="sr-only"> (current)</span></a></h1>

      <a href="https://sandro.mathys.io/" class="nav-item nav-link">Resume</a>

  </div>
</nav>

<main class="main">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>Sandro Mathys | Blog</title>
  <subtitle>Posts filed in OpenStack</subtitle>
  <id>https://blog.mathys.io/</id>
  <link href="https://blog.mathys.io/"/>
  <link href="https://blog.mathys.io/tags/openstack/atom.xml" rel="self"/>
  <updated>2014-12-10T09:49:00+01:00</updated>
  <author>
    <name>Sandro Mathys</name>
  </author>
  <entry>
    <title>Fedora 19 released, now with official cloud images</title>
    <link rel="alternate" href="https://blog.mathys.io/2013/07/03/fedora-19-released-now-with-official-cloud-images/"/>
    <id>https://blog.mathys.io/2013/07/03/fedora-19-released-now-with-official-cloud-images/</id>
    <published>2013-07-03T16:33:00+02:00</published>
    <updated>2018-05-11T15:13:42+02:00</updated>
    <author>
      <name>Sandro Mathys</name>
    </author>
    <content type="html">&lt;p&gt;Earlier this week, Fedora 19 ("Schrödinger's Cat") has been released. If you've missed the news, here's the &lt;a href="https://lists.fedoraproject.org/pipermail/announce/2013-July/003167.html"&gt;announcement&lt;/a&gt; by Fedora Project Leader Robyn Bergeron and to be complete, here's the &lt;a href="https://docs.fedoraproject.org/en-US/Fedora/19/html/Release_Notes/index.html"&gt;release notes&lt;/a&gt;, too.&lt;/p&gt;

&lt;p&gt;As with ever new Fedora release, there's a load of new &lt;a href="https://fedoraproject.org/wiki/Releases/19/FeatureList"&gt;features&lt;/a&gt;. But in all those linked pages (and most press articles, too) one thing is easily missed or even completely absent that might be of interest to all users and operators of clouds (be it OpenStack, Eucalyptus, CloudStack or any other).&lt;/p&gt;

&lt;h3&gt;Fedora 19 Cloud Images&lt;/h3&gt;

&lt;p&gt;While providing AMI images inside Amazon EC2 for a while already, Fedora 19 is the first release to also feature official raw and qcow2 images ready to run in your own public or private cloud!&lt;/p&gt;

&lt;p&gt;From the release notes, &lt;a href="https://docs.fedoraproject.org/en-US/Fedora/19/html/Release_Notes/sect-Release_Notes-Changes_for_Sysadmin.html#sect_Cloud"&gt;section Cloud&lt;/a&gt; (2.7.1):&lt;/p&gt;

&lt;blockquote&gt;&lt;p&gt;Ready-to-run cloud images are provided as part of Fedora 19. These are available in Amazon EC2 or for direct download. The downloadable images are available in compressed raw image format and in qcow2 for immediate use with EC2, OpenStack, CloudStack, or Eucalyptus. The images are configured with cloud-init, and so will take advantage of ec2-compatible metadata services for provisioning SSH keys.&lt;/p&gt;&lt;/blockquote&gt;

&lt;p&gt;So what are you waiting for? Get them, while they're hot! Quick link: &lt;a href="https://cloud.fedoraproject.org/"&gt;cloud.fedoraproject.org&lt;/a&gt;&lt;/p&gt;

&lt;h3&gt;Import into OpenStack&lt;/h3&gt;

&lt;p&gt;For fellow OpenStack operators who wish to import Fedora 19 into Glance, the OpenStack Image Service, let me provide an example on how to easily import an image, e.g. x86_64 / qcow2:&lt;br&gt;&lt;/p&gt;

&lt;kbd&gt;glance image-create --name "Fedora 19 x86_64" --is-public true --container-format bare --disk-format qcow2 --copy-from http://download.fedoraproject.org/pub/fedora/linux/releases/19/Images/x86_64/Fedora-x86_64-19-20130627-sda.qcow2&lt;/kbd&gt;

&lt;h3&gt;Or click your way through Horizon instead&lt;/h3&gt;

&lt;p&gt;If you run OpenStack Grizzly or Havana, it's also possible to import the image easily through Horizon, the OpenStack Dashboard.&lt;/p&gt;

&lt;p&gt;In your &lt;code&gt;Project&lt;/code&gt; view, just go to  &lt;code&gt;Images &amp;amp; Snapshots&lt;/code&gt; and click the &lt;code&gt;Create Image&lt;/code&gt; button in the top right corner. Again, specify a &lt;code&gt;Name&lt;/code&gt; (e.g. Fedora 19 x86_64), provide the &lt;code&gt;Image Location&lt;/code&gt; by &lt;a href="https://download.fedoraproject.org/pub/fedora/linux/releases/19/Images/x86_64/Fedora-x86_64-19-20130627-sda.qcow2"&gt;URL&lt;/a&gt;, choose &lt;code&gt;QCOW2&lt;/code&gt; as the &lt;code&gt;Format&lt;/code&gt; and decide whether you want to make this image &lt;code&gt;Public&lt;/code&gt; to all users of your cloud or only to your very own tenant. The &lt;code&gt;Minimum Disk&lt;/code&gt; and &lt;code&gt;Minimum RAM&lt;/code&gt; parameters are optional.&lt;/code&gt;

&lt;p&gt;Now, &lt;code&gt;Create Image&lt;/code&gt; and, depending on your bandwidth to the closest mirror, Fedora 19 will soon be available to be launched!&lt;/p&gt;
</content>
  </entry>
  <entry>
    <title>Unused base images in OpenStack Nova</title>
    <link rel="alternate" href="https://blog.mathys.io/2013/03/27/unused-base-images-in-openstack-nova/"/>
    <id>https://blog.mathys.io/2013/03/27/unused-base-images-in-openstack-nova/</id>
    <published>2013-03-27T16:36:00+01:00</published>
    <updated>2018-05-11T15:13:42+02:00</updated>
    <author>
      <name>Sandro Mathys</name>
    </author>
    <content type="html">&lt;p&gt;By default, unused base images are left behind in &lt;code&gt;/var/lib/nova/instances/_base/&lt;/code&gt; indefinitely. Some people like it that way, probably because additional instances (on the same node) will launch quicker. Some people don't like it, maybe because they are limited on space even though that's considered cheap nowadays. I'm still a bit twisted, having limited storage in my proof of concept cloud but valuing short spawn times.&lt;/p&gt;

&lt;p&gt;But ever since (I learnt that) removal of unused base images was set to True by default, I wondered why they'd still remain for me. Did I do something wrong? Being new to this I had not the slightest clue, except there were no errors which is generally a good sign you're doing something right. But then &lt;a href="http://kashyapc.com/2013/03/03/openstack-nova-dealing-with-unused-base-images/"&gt;Kashyap Chamarthy&lt;/a&gt; wrote an interesting article on the matter a few weeks ago. Still, something seemed either wrong or missing or just different in my installation (likely one of the latter two and probably a detail, but an important one). So I started to poke around the topic myself this morning, which included reading some code to help me understand things better. And to find the actual default values, because the documentation is often wrong about those.&lt;/p&gt;

&lt;h3&gt;Configuration File Options&lt;/h3&gt;

&lt;p&gt;There's four options that you can set in &lt;code&gt;/etc/nova/nova.conf&lt;/code&gt; that do matter here, most of them being specific to Nova Compute. The following lists the options with their default values and adds a brief explanation (the first sentence always being based on the actual help text of the option).&lt;/p&gt;

&lt;p&gt;&lt;samp&gt;remove_unused_base_images = True&lt;/samp&gt;&lt;br&gt;
Should unused base images be removed?&lt;/p&gt;

&lt;p&gt;&lt;samp&gt;periodic_interval = 60&lt;/samp&gt;&lt;br&gt;
How many seconds to wait between running periodic tasks. This is the definition of ticks (see below) and used by several Nova services. I think changing this value requires you to restart all of them on the same node.&lt;/p&gt;

&lt;p&gt;&lt;samp&gt;image_cache_manager_interval = 0&lt;/samp&gt;&lt;br&gt;
Number of periodic scheduler ticks to wait between runs of the image cache manager. Obviously that's the periodic task which will actually perform the image removal. If set to 0, the cache manager will not be started!&lt;/p&gt;

&lt;p&gt;&lt;samp&gt;remove_unused_original_minimum_age_seconds = 86400&lt;/samp&gt;&lt;br&gt;
Unused unresized base images younger than this will not be removed. The age refers to the file's mtime.&lt;/p&gt;

&lt;p&gt;&lt;samp&gt;remove_unused_resized_minimum_age_seconds = 3600&lt;/samp&gt;&lt;br&gt;
Unused resized base images younger than this will not be removed. The age refers to the file's mtime.&lt;/p&gt;

&lt;p&gt;So if you want to disable automatic removal of base images, you should be fine because the Cache Manager is disabled by default. To be sure, you might want to change the first of those options to False.&lt;/p&gt;

&lt;p&gt;But should you wish to enable it, you must define an interval for the Cache Manager. Not sure what good values are, though but I guess that depends on how your cloud is used, anyway.&lt;/p&gt;

&lt;p&gt;Once the Cache Manager enabled, you might want to tweak the last two options a bit though the defaults are sane to start with. Basically you have two kinds of base images and one option for each: 1) original, or unresized. That's basically the uncompressed COW image. 2) resized. That's the original image but extended to reflect the (chosen) flavor's disk size. Note: depending on your configuration, you might not have both or they might be in different formats, not sure. See the excellent and very complete discussion of this topic: &lt;a href="https://www.pixelbeat.org/docs/openstack_libvirt_images/"&gt;Pádraig Brady&lt;/a&gt;.&lt;/p&gt;

&lt;h3&gt;What's actually happening&lt;/h3&gt;

&lt;p&gt;Let me try to make the complete process a little bit clearer still. Once Nova Compute is started (and after a configurable random delay to prevent a stampede), the periodic task manager is started in order to count the seconds between ticks. Every X (the interval you set) ticks, the cache manager is run. First check it does is what images are not being used in any instances anymore. Those that are still in use are ignored, the others are checked for their age which is calculated as seconds since the base file's mtime (i.e. time of the last modification of the file). If the age is smaller than what you configured (for this kind of base image: original or resized), it's deemed "too young" and left alone. But if it's mature enough, it's deleted. That's it already, end of story. The process can easily be obeyed in the log file, to understand it even better.&lt;/p&gt;

&lt;h3&gt;Shared &lt;code&gt;instances&lt;/code&gt; Folder&lt;/h3&gt;

&lt;p&gt;If you have configured live migration of instances, all your compute nodes share one common &lt;code&gt;/var/lib/nova/instances/&lt;/code&gt; and you might wonder what's different in that case. Of course the process is exactly the same with one important difference: base images that are unused on one node could still be in use on a different node and should not be removed. But Nova Compute won't know it's used somewhere else and the Cache Manager will try to remove the image nevertheless. Luckily, the mtime changes now and then if the image is in usage on any node so unless you set the age options to a very low value, you should be fine. In a test run I once had it down to 4 minutes and that still worked. With the idle Cirros 0.3.0 image I spawned, the mtime changed ever ~2 minutes. That said, I don't yet completely understand why it changes at all.&lt;/p&gt;

&lt;h3&gt;Grizzly&lt;/h3&gt;

&lt;p&gt;It's important to note that the above is probably valid for Folsom only.&lt;/p&gt;

&lt;p&gt;&lt;b&gt;Update:&lt;/b&gt; In Grizzly, there is no &lt;code&gt;periodic_interval&lt;/code&gt; anymore, the replacement is more dynamic and can probably be ignored. That also means &lt;code&gt;image_cache_manager_interval&lt;/code&gt; is now using seconds as there are no fixed ticks anymore. More importantly, though: its value is now set to 2400 by default, i.e. it's enabled. I don't think anything else changed.&lt;/p&gt;
</content>
  </entry>
  <entry>
    <title>Setting a user password when launching cloud images</title>
    <link rel="alternate" href="https://blog.mathys.io/2013/07/05/setting-a-user-password-when-launching-cloud-images/"/>
    <id>https://blog.mathys.io/2013/07/05/setting-a-user-password-when-launching-cloud-images/</id>
    <published>2013-07-05T16:11:00+02:00</published>
    <updated>2018-05-11T15:13:42+02:00</updated>
    <author>
      <name>Sandro Mathys</name>
    </author>
    <content type="html">&lt;p&gt;If you're using e.g. the new Fedora 19 cloud images (see &lt;a href="https://blog.mathys.io/2013/07/fedora-19-released-now-with-official-cloud-images.html"&gt;my previous post&lt;/a&gt;) you might have noticed that logging in with a password is disabled, both as root or the default user fedora. Usually, that's no issue and actually a security feature. Injecting and using SSH keys instead is the accepted solution. But that's not what I'm here to discuss today.&lt;/p&gt;

&lt;p&gt;Still, sometimes logging in over SSH does not do the job. Maybe networking in your cloud is broken and you need access to a guest to further debug it. But no networking, no SSH login. Fortunately, you can use (no)VNC and a tty to log in, right? Well, except SSH keys don't work there. Hence you need the user (or root) to accept password based logins.&lt;/p&gt;

&lt;h3&gt;Cloud-Init&lt;/h3&gt;

&lt;p&gt;Luckily, Fedora 19 like most other modern cloud images uses cloud-init and thereby supports userdata (which basically is user-provided metadata). Now, with userdata, you can write a simple "script" (it's actually a YAML-style config file) to set a password. By default, that password can be used only just once and needs to be changed upon login. Unless you diable the expiration with another parameter. And if you want enable password login over SSH, there's a parameter for that as well. So putting all together, your userdata script could look like this:&lt;br&gt;
&lt;samp&gt;#cloud-config&lt;br&gt;
password: mysecret&lt;br&gt;
chpasswd: { expire: False }&lt;br&gt;
ssh_pwauth: True&lt;/samp&gt;&lt;/p&gt;

&lt;p&gt;Please note, that the first line is &lt;b&gt;not&lt;/b&gt; a comment but actually a required "keyword".&lt;/p&gt;

&lt;p&gt;Now, there's as many ways to provide the cloud image with the userdata as there are different ways to launch a cloud image. Let me cover what I know.&lt;/p&gt;

&lt;h3&gt;Horizon&lt;/h3&gt;

&lt;p&gt;If you're launching your instances through Horizon, the OpenStack Dashboard, you go to &lt;code&gt;Instances&lt;/code&gt;, click the &lt;code&gt;Launch Instance&lt;/code&gt; button, do your usual settings, go to the &lt;code&gt;Post-Creation&lt;/code&gt; tab and insert the above code as a &lt;code&gt;Customization Script&lt;/code&gt;. Hit the &lt;code&gt;Launch&lt;/code&gt; button and that's it. Once the instance is up, you should be able to log in with the configured password.&lt;/p&gt;

&lt;h3&gt;Nova CLI&lt;/h3&gt;

&lt;p&gt;On the command line, you need to create a text file with the code above. Then, you just give that &lt;code&gt;nova boot&lt;/code&gt; command a &lt;code&gt;--user-data &amp;lt;myscript&amp;gt;&lt;/code&gt; parameter and there you go. Again, once the instance is up, you should be able to log in with the configure password.&lt;/p&gt;

&lt;h3&gt;Other clouds, other tools and the APIs&lt;/h3&gt;

&lt;p&gt;Right, obviously the userdata mechanism isn't exclusive to OpenStack. I'm certain Amazon EC2 does it too (probably did it first) and so might other cloud stacks like Eucalyptus. Also, other tools than the Nova CLI do support it, e.g. the euca2ools. And both the OpenStack Compute API and the EC2 API, probably among others, do support it, too. Unfortunately, my experience and knowledge are limited and therefore I'll have to send you to the respective documentation or support channels. But as long as your cloud image is using (a current version of) cloud-init, the above script should work independent of the underlying solutions. After all, isn't that the purpose of true cloud computing?&lt;/p&gt;
</content>
  </entry>
</feed>

      </div>
    </div>
  </div>
</main>
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-nowrap text-xs-center">
            <small class="text-muted">Powered by <a href="https://gitlab.com/">GitLab</a>, <a href="https://middlemanapp.com/">Middleman</a> &amp; <a href="https://getbootstrap.com/">Bootstrap</a>.</small>
          </div>
        </div>
      </div>
    </footer>
      <script src="/js/disqus.js" defer></script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>-->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>-->
  </body>
</html>

